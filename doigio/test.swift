//
//  test.swift
//  doigio
//
//  Created by Nguyen Kien on 2/3/16.
//  Copyright © 2016 Nguyen Kien. All rights reserved.
//

import UIKit

class test: UIView {

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.view = UINib(nibName: "test", bundle: NSBundle(forClass: self.dynamicType)).instantiateWithOwner(self, options: nil)[0] as? UIView
        addSubview(self.view)

    }


}
