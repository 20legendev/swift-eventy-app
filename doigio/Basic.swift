//
//  Basic.swift
//  doigio
//
//  Created by Nguyen Kien on 2/4/16.
//  Copyright © 2016 Nguyen Kien. All rights reserved.
//

import Foundation

func setTimeout(delay:NSTimeInterval, block:()->Void) -> NSTimer {
    return NSTimer.scheduledTimerWithTimeInterval(delay, target: NSBlockOperation(block: block), selector: "main", userInfo: nil, repeats: false)
}

func setInterval(interval:NSTimeInterval, block:()->Void) -> NSTimer {
    return NSTimer.scheduledTimerWithTimeInterval(interval, target: NSBlockOperation(block: block), selector: "main", userInfo: nil, repeats: true)
}

extension CGRect {
    var wh: (w: CGFloat, h: CGFloat) {
        return (size.width, size.height)
    }
}

extension CAGradientLayer {
    class func gradientLayerForBounds(bounds: CGRect, fromColor: UIColor, toColor: UIColor) -> CAGradientLayer {
        let layer = CAGradientLayer()
        layer.frame = bounds
        layer.colors = [fromColor.CGColor, toColor.CGColor]
        return layer
    }
}