//
//  ViewController.swift
//  doigio
//
//  Created by Nguyen Kien on 1/27/16.
//  Copyright © 2016 Nguyen Kien. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    let cellName = "EventCell"
    var data = ["Vở kịch: Hoàng gia và những người bạn", "Ăn cơm có thịt tại thị trấn Sapa"]
    var sub = ["Bên tôi mỗi khi buồn, lặng lẽ xoá tan âu lo", "Gạt giọt nước mắt thăng trầm"]
    
    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        setupLogo()
        let nib = UINib(nibName: "EventCell", bundle: nil)
        tableView.registerNib(nib, forCellReuseIdentifier: "EventCellNib")
    }
    
    func setupLogo(){
        let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 94, height: 22))
        imageView.contentMode = .ScaleAspectFit
        let image = UIImage(named: "eventy.png")
        imageView.image = image
        navigationItem.titleView = imageView
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        // let cell = tableView.dequeueReusableCellWithIdentifier(cellName, forIndexPath: indexPath)
        let cell:EventCell = tableView.dequeueReusableCellWithIdentifier("EventCellNib") as! EventCell
        cell.titleView.text = data[indexPath.row % 2]
        cell.eventSubtitle.text = sub[indexPath.row % 2]
        if indexPath.row % 2 == 1{
            cell.iconView.image = UIImage(named: "london")
        }else{
            cell.iconView.image = UIImage(named: "paris")
        }
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        performSegueWithIdentifier("eventDetailSegue", sender: nil)
    }

    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        let destinationVC = segue.destinationViewController as! EventDetailController
        navigationItem.title = ""
        destinationVC.eventName = "Đêm nhạc điện tử với nhóm La Fine Equipe"
    }

}

