//
//  PropertyViewCell.swift
//  doigio
//
//  Created by Nguyen Kien on 2/2/16.
//  Copyright © 2016 Nguyen Kien. All rights reserved.
//

import UIKit

protocol PropertyViewCellProtocol{
    func propetyViewCellChangeSize(item: PropertyViewCell, size: CGSize);
}

@IBDesignable class PropertyViewCell: UIView {
    
    var delegate: PropertyViewCellProtocol?
    let nibName = "PropertyViewCell"
    @IBOutlet weak var propLabel: UILabel!
    @IBOutlet weak var contentLabel: UILabel!
    var contentView: UIView?
    
    @IBInspectable var labelText: String? {
        get {
            return propLabel.text
        }
        set(labelText) {
            propLabel.text = labelText
        }
    }
    
    @IBInspectable var contentText: String? {
        get {
            return contentLabel.text
        }
        set(contentText) {
            contentLabel.text = contentText
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    override func layoutSubviews(){
        super.layoutSubviews()
        resize()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    func resize(){
        var frame = contentView?.frame
        frame?.size.height = contentLabel.frame.height + 16;
        contentView?.frame = frame!
        self.delegate?.propetyViewCellChangeSize(self, size: CGSizeMake(contentLabel.frame.width, contentLabel.frame.height + 16))
    }
    
    func setup() {
        let bundle = NSBundle(forClass: self.dynamicType)
        let nib = UINib(nibName: nibName, bundle: bundle)
        contentView = nib.instantiateWithOwner(self, options: nil).first as? UIView
        addSubview(contentView!)
        
        contentView!.translatesAutoresizingMaskIntoConstraints = false
        
        let bindings = ["view": contentView!]
        addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("H:|[view]|", options:NSLayoutFormatOptions(rawValue: 0), metrics:nil, views: bindings))
        addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("V:|[view]|", options:NSLayoutFormatOptions(rawValue: 0), metrics:nil, views: bindings))
    }
}