//
//  EventCell.swift
//  doigio
//
//  Created by Nguyen Kien on 1/27/16.
//  Copyright © 2016 Nguyen Kien. All rights reserved.
//

import UIKit

class EventCell: UITableViewCell {

    @IBOutlet weak var titleView: UILabel!
    @IBOutlet weak var eventSubtitle: UILabel!
    @IBOutlet weak var iconView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

}
