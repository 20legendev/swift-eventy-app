//
//  JoinEventBar.swift
//  doigio
//
//  Created by Nguyen Kien on 2/4/16.
//  Copyright © 2016 Nguyen Kien. All rights reserved.
//

import UIKit

@IBDesignable class JoinEventBar: UIView {
    var contentView: UIView?
    var nibName = "JoinEventBar"
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
        
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    func setup() {
        let bundle = NSBundle(forClass: self.dynamicType)
        let nib = UINib(nibName: nibName, bundle: bundle)
        contentView = nib.instantiateWithOwner(self, options: nil).first as? UIView
        addSubview(contentView!)
        
        contentView!.translatesAutoresizingMaskIntoConstraints = false
        
        let bindings = ["view": contentView!]
        addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("H:|[view]|", options:NSLayoutFormatOptions(rawValue: 0), metrics:nil, views: bindings))
        addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("V:|[view]|", options:NSLayoutFormatOptions(rawValue: 0), metrics:nil, views: bindings))
    }

}
