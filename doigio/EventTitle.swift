//
//  EventTitle.swift
//  doigio
//
//  Created by Nguyen Kien on 2/15/16.
//  Copyright © 2016 Nguyen Kien. All rights reserved.
//

import Foundation

import UIKit

@IBDesignable class EventTitle: UIView {
    
    let nibName = "EventTitle"
    var contentView: UIView?
    
    @IBOutlet weak var year: UILabel!
    @IBOutlet weak var date: UILabel!
    @IBOutlet weak var month: UILabel!
    
    @IBInspectable var monthText: String? {
        get {
            return month.text
        }
        set(monthText) {
            month.text = monthText
        }
    }
    
    @IBInspectable var dateText: String? {
        get {
            return date.text
        }
        set(dateText) {
            date.text = dateText
        }
    }
    @IBInspectable var yearText: String? {
        get {
            return year.text
        }
        set(yearText) {
            year.text = yearText
        }
    }
    /*
    @IBInspectable var titleText: String? {
        get {
            return title.text
        }
        set(titleText) {
            title.text = titleText
        }
    }
    */
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    func setup() {
        let bundle = NSBundle(forClass: self.dynamicType)
        let nib = UINib(nibName: nibName, bundle: bundle)
        contentView = nib.instantiateWithOwner(self, options: nil).first as? UIView
        addSubview(contentView!)
        
        contentView!.translatesAutoresizingMaskIntoConstraints = false
        
        let bindings = ["view": contentView!]
        addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("H:|[view]|", options:NSLayoutFormatOptions(rawValue: 0), metrics:nil, views: bindings))
        addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("V:|[view]|", options:NSLayoutFormatOptions(rawValue: 0), metrics:nil, views: bindings))
    }
}