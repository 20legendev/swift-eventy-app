//
//  EventDetailController.swift
//  doigio
//
//  Created by Nguyen Kien on 1/28/16.
//  Copyright © 2016 Nguyen Kien. All rights reserved.
//

import Foundation
import UIKit

let headerHeight:CGFloat = 64
class EventDetailController: UIViewController, PropertyViewCellProtocol{
    
    @IBOutlet weak var endViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var startViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var addressHeightContraint: NSLayoutConstraint!
    @IBOutlet weak var addressView: PropertyViewCell!
    @IBOutlet weak var endView: PropertyViewCell!
    @IBOutlet weak var startView: PropertyViewCell!
    @IBOutlet weak var scrollView: UIScrollView!
    
    var eventName = ""
    let imageOriginHeight: CGFloat = 200
    
    @IBOutlet weak var featureImage: UIImageView!
    // @IBOutlet weak var featuredImageView: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.featureImage.image = UIImage(named: "test")
        addressView.delegate = self
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        initScreen()
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.navigationBar.barStyle = .Default
        navigationController?.navigationBar.setBackgroundImage(nil, forBarMetrics: UIBarMetrics.Default)
    }
    
    func initScreen(){
        // self.navigationItem.title = eventName
        let navBar: UINavigationBar! = navigationController?.navigationBar
        navBar.shadowImage = UIImage()
        navBar.translucent = true
        navBar.backgroundColor = UIColor.clearColor()
        self.navigationController?.view.backgroundColor = UIColor.clearColor()
        navBar.barStyle = .BlackTranslucent
        navBar.setBackgroundImage(imageLayerForGradientBackground(), forBarMetrics: UIBarMetrics.Default)
    }
    
    func scrollViewDidScroll(scrollView: UIScrollView!) {
        let yPos: CGFloat = -scrollView.contentOffset.y
        if (yPos > 0) {
            var imgRect: CGRect = self.featureImage.frame
            imgRect.origin.y = scrollView.contentOffset.y
            imgRect.size.height = imageOriginHeight + yPos
            self.featureImage.frame = imgRect
        }
    }
    
    override func preferredStatusBarStyle() -> UIStatusBarStyle {
        return .LightContent
    }
    
    override func prefersStatusBarHidden() -> Bool {
        return false
    }
}

// delegate to child view changes
extension EventDetailController{
    func propetyViewCellChangeSize(item: PropertyViewCell, size: CGSize){
        addressHeightContraint.constant = size.height
    }
    
    private func imageLayerForGradientBackground() -> UIImage {
        var updatedFrame = self.navigationController?.navigationBar.bounds
        updatedFrame!.size.height += 20
        var layer = CAGradientLayer.gradientLayerForBounds(updatedFrame!, fromColor: UIColor(red: 0, green: 0,blue: 0, alpha: 1), toColor: UIColor(red: 0, green: 0, blue: 0, alpha: 0))
        UIGraphicsBeginImageContext(layer.bounds.size)
        layer.renderInContext(UIGraphicsGetCurrentContext()!)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image
    }
}

/*
extension EventDetailController{
    
    func showMap(){
        let lat = 21.01062775
        let  positionCenter = CLLocationCoordinate2DMake(lat + 0.0009, 105.8493042)
        mapView.camera = GMSCameraPosition(target: positionCenter, zoom: 16, bearing: 0, viewingAngle: 0)
        mapView.settings.setAllGesturesEnabled(false)
        
        let center = GMSMarker(position: positionCenter)
        center.title = "Hello World"
        center.icon = UIImage(named: "marker")
        center.map = mapView
    }
    
    func showMarker() -> UIImageView{
        let markerBackground = UIImageView(frame: CGRectMake(0, 0, 30, 46))
        markerBackground.image = UIImage(named: "marker")
        markerBackground.contentMode = UIViewContentMode.ScaleAspectFill
        return markerBackground
    }
}
*/

